# Endpoints

For details on how the client authenticates with the server, see
[Authentication](authentication.md).

**All endpoints must be available over HTTPS.**

If a method is not defined here, the server must return a string representation
of a JSON [`Error`](types.md#error) object with the error value
[`invalid_method`](errors.md#invalid_method).

## Data

### GET

Returns a string representation of the JSON object that the user has
stored on the server.

If the user does not have any saved data, then a string representation of a JSON
[`Error`](types.md#error) object with the error value
[`no_data`](errors.md#no_data).

### POST

The body of the request is a string representation of a JSON object containing
the data that the client wants to store on the server.

If the request is succesful, no data needs to be returned.

If the body is not a valid string representation of a JSON object, the server
can return a string representation of a JSON [`Error`](types.md#error) object
with the error value [`invalid_data`](errors.md#invalid_data) instead of saving the
data.

### DELETE

This request is used to clear the user's saved data.

If the request is succesful, no data needs to be returned.

## Delete

### POST

This request is used to clear the user's saved data.

Differently from the [DELETE request to the Data endpoint](endpoints.md#delete),
this request also makes ALL [`Token`](types.md#token)s issued to the user
invalid (meaning the user has to log in again to access their data).

Additionally, if the value of `accountDeleteSupported` in the server's
[index file](index-file.md) is `true`, this request also deletes the user's
account entirely from the server.

## Logout

### POST

This request invalidates the [`Token`](types.md#token) used to make the request.
Other [`Token`](types.md#token)s issued to the user remain valid and
can still be used.