# Errors

## invalid_data

The data that the client is attempting to save on the server is not a valid
string representation of a JSON object.

**HTTP error code**: `400` (or `415` if the `Content-Type` header is not
`application/json`)

## invalid_token

The [`Token`](types.md#token) provided by the client cannot be used to
authenticate (eg. it does not exist or has been invalidated by a
previous request).

**HTTP error code**: `403`

## missing_token

The client did not provide a [`Token`](types.md#token).

**HTTP error code**: `403`

## no_data

The user hasn't stored any data on the server.

**HTTP error code**: `404`

## storage_limit_reached

The sevrer is refusing to save the data that the client has provided because it
is too large.

**HTTP error code**: `413`