# Authentication

The purpose of a *gianniSync* server is to store string representations of JSON
objects for its users.

To do this, users must authenticate and they must also be able to be
distinguished from one another.

To log in, the user visits your server's website or the URL defined in the
`login` key of the [index file](index-file.md) to log in.

The way that the server logs a user in is not defined in this specification and
is left up to the server itself.

Once finished, the user is given a [`SetupCode`](types.md#setupcode) to input
into their client.

If the `giannisyncRedirect` key was present at the start of
the login flow, then the user should be instead redirected to that URL, with the
`giannisyncSetupCode` value added to the URL's query string. This redirect URL
may use any scheme, not just `http` or `https`.  
For example, if `giannisyncRedirect` had a value of
`https://example.com/redirect` and the [`SetupCode`](types.md#setupcode) is
`ExampleSetupCode`, then the user should be redirected to
`https://example.com/redirect?giannisyncSetupCode=ExampleSetupCode`.

The [`SetupObject`](types.md#setupobject) contained in the
[`SetupCode`](types.md#setupcode) given to the user contains a
[`Token`](types.md#token). This is a `string` which does not have a defined
format, it is up to the server to use whatever. It will be used by the client
with no variations.

All requests to the various [endpoints](endpoints.md) will contain a header with
the name `X-Giannisync-Token` and, as value, the [`Token`](types.md#token)
contained in the [`SetupCode`](types.md#setupcode) that the user inputted into
their client.

If a request is made to an [endpoint](endpoints.md) without a token, the server
must return a string representation of a JSON [`Error`](types.md#error) object
with the error value [`missing_token`](errors.md#missing_token) instead of
carrying out the request.

If a request is made to an [endpoint](endpoints.md) with a token that is not or
no longer is valid, the server must return a string representation of a JSON
[`Error`](types.md#error) object with the error value
[`invalid_token`](errors.md#invalid_token) instead of carrying out the request.