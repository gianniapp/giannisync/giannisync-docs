# CORS

Since *gianni mangia una mela* is a game based on HTML, all web requests made by
it must contain CORS information and all servers must support CORS.

**Since** ***gianni mangia una mela*** **adds additional headers to all**
**requests,**
**[CORS preflight requests](https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request)**
**must be supported.**

## Access-Control-Allow-Origin

The `Access-Control-Allow-Origin` header must be `*` to allow requests from any
origin, since the versions of the game that can be installed on various devices
are considered to be different origins by their respective platforms.

Also, anyone can host their copy of the game, so we must not restrict requests
to just the official online version
([play.gianni.app](https://play.gianni.app)).

## Access-Control-Allow-Headers

The client attaches various non-standard headers to all requests:

| Header                        | Description                                                                                        |
| ----------------------------- | -------------------------------------------------------------------------------------------------- |
| `X-Gianniapp-Base-Version`    | Version of `gianniapp-base` used by the client. Not present if the client is not the game.         |
| `X-Gianniapp-Middler-Version` | Version of the middler used by the client, or version of the client if the client is not the game. |
| `X-Gianniapp-Middler-Name`    | Name of the middler used by the client, or name of the client if the client is not the game.       |
| `X-Giannisync-Version`        | *gianniSync* spec version supported by the client.                                                 |
| `X-Giannisync-Token`          | The [`Token`](types.md#token) used by the client for authentication.                               |

These headers must be listed in the `Access-Control-Allow-Headers` header for
ALL requests, including those to the [index file](index-file.md).