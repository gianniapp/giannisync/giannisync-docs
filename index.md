# gianniSync

**This is the in-development spec. See**
**[this repository's tags](https://gitlab.com/gianniapp/giannisync/giannisync-docs/-/tags)**
**for the spec used in the current version of the game.**

- [Authentication](authentication.md)
- [CORS](cors.md)
- [Domain linking](domain-linking.md)
- [Endpoints](endpoints.md)
- [Errors](errors.md)
- [Index file](index-file.md)
- [Types](types.md)