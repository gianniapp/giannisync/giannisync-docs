# Types

## DomainString

A `string` that consists of a domain name (or subdomain).

Examples: `gianni.app`, `sync.gianni.app`

## Error

An `object` with a single key: `error`.

This key must be set to the [error value](errors.md).

## Localized

An `object` used to localize a string in various languages.

Its keys are the language codes and the values are `string`s that consist of the
localized content.

Optionally, the key `default`, with the value being a `string` of a language
code, can be set to define which language's content should be displayed by the
client if it cannot find a `string` for its own language.

Example (JSON representation):
```json
{
  "default": "en",
  "en" : "English value",
  "it": "Valore in italiano"
}
```
In this example, if the client is set to English it will display
`English value`, if it is set to Italian it will display `Valore in italiano`,
and if it is set to a different language it will display the English string
(`English value`).

If the `default` key is not set and the client cannot find a `string` for its
own language, it will try to find keys for the following languages, in this
order:
- [`en`] English
- [`it`] Italian

## SetupCode

A `string` that consists of a Base64-encoded JSON string representation of
a [`SetupObject`](types.md/#setupobject).

## SetupObject

An `object` with the following keys (all are required):

| Key     | Value                              | Description                                                  |
| ------- | ---------------------------------- | ------------------------------------------------------------ |
| `url`   | [`URLString`](types.md/#urlstring) | URL to the server's [index file](index-file.md).             |
| `token` | [`Token`](types.md/#token)         | The token that the client will use to authenticate requests. |

## Token

An arbitrary `string` that is associated to the user and is used to authenticate
them.

Please see [Authentication](authentication.md) for more information on
authentication.

## URLString

A `string` that consists of a URL.

All URLs must use the `https:` scheme.