# Domain linking

The client may validate DNS lookups with DNSSEC, so it must be supported by the
linked domain's nameserver(s) and it must be properly configured.

## Pointing vs. linking

Pointing a domain to an index file creates a shortcut: a user can type the
domain name into their client to view information about the server and log in
instead of needing to find the server's website and its login page manually.

Linking provides the same benefits as pointing along with the domain then
becoming a sort of "official home" of a *gianniSync* server.  
Linking a domain over simply pointing to it may not have any significant
benefits now, but it may in future versions of *gianniSync*.

For example, a website providing *gianniSync* as a feature for registered users
may not want the main website domain to be linked to the
[index file](index-file.md) (and instead link a subdomain, such as
`giannisync.example.com`), but it may want the main domain to be pointed to the
[index file](index-file.md), so that, in case a user does not type the full
`giannisync.example.com` and types `example.com` instead, the client can still
find the [index file](index-file.md).

## Pointing a domain to an [index file](index-file.md)

A record must be created with the name `_giannisync-index` with the value being
`giannisync-index=` followed by the URL to the index file, which must use the
`https:` scheme.

Example:
`_giannisync-index.example.com TXT giannisync-index=https://example.com/giannisync-index.json`

Multiple domains can point to the same index file.

## Linking a domain and an [index file](index-file.md)

To link a domain and an [index file](index-file.md), the domain must first be
pointed to the file using the procedure described above.

To complete linking, a string consisting of the domain name must be set as the
value of the `domain` key in the [index file](index-file.md).

Only one domain may be linked to a particular [index file](index-file.md).