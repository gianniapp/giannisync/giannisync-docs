# Index file

A *gianniSync* server is defined by its index file.

This file tells the client information about the server and the address to all
of the server's endpoints.

**The index file must be available over HTTPS.**

All keys must be present.

| Key                      | Value                                                    | Description                                                                                                                          |
| ------------------------ | -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `version`                | `int` 2                                                  | *gianniSync* specification version used by the server.                                                                               |
| `name`                   | `string` or [`Localized`](types.md/#localized)           | Name of the server.                                                                                                                  |
| `description`            | `string` or [`Localized`](types.md/#localized)           | Description of the server.                                                                                                           |
| `domain`                 | [`DomainString`](types.md/#domainstring) or `bool` false | Domain that the file is linked to. `bool` false if this file isn't linked to a domain. [More on domain linking](domain-linking.md)   |
| `homepage`               | [`URLString`](types.md/#urlstring)                       | URL of the server's homepage, where a user can get more information.                                                                 |
| `privacy`                | [`URLString`](types.md/#urlstring) or `bool` false       | URL of the server's privacy policy. `bool` false if the server doesn't have one.                                                     |
| `icon`                   | [`URLString`](types.md/#urlstring)                       | URL to a PNG file that is the server's icon.                                                                                         |
| `signup`                 | [`URLString`](types.md/#urlstring) or `bool` false       | URL of a webpage where a user can create an account. `bool` false if new accounts cannot be created (eg. a private server)           |
| `login`                  | [`URLString`](types.md/#urlstring)                       | URL of a webpage where a user can log into their account and get a [`SetupCode`](types.md/#setupcode).                               |
| `delete`                 | [`URLString`](types.md/#urlstring)                       | URL of the [`Delete`](endpoints.md/#delete) endpoint.                                                                                |
| `logout`                 | [`URLString`](types.md/#urlstring)                       | URL of the [`Logout`](endpoints.md/#logout) endpoint.                                                                                |
| `dataEndpoint`           | [`URLString`](types.md/#urlstring)                       | URL of the [`Data`](endpoints.md/#data) endpoint.                                                                                    |
| `accountDeleteSupported` | `bool`                                                   | `true` if the server's [`Delete`](endpoints.md/#delete) endpoint also deletes the user's account from the server, `false` otherwise. |

Example:
```json
{
  "version": 1,
  "name": "My sync server",
  "description": {
    "default": "en",
    "en": "This is a gianniSync server",
    "it": "Questo è un server gianniSync"
  },
  "homepage": "https://example.com",
  "privacy": "https://example.com/privacy",
  "icon": "https://example.com/icon.png",
  "signup": "https://example.com/signup",
  "login": "https://example.com/login",
  "logout": "https://example.com/api/logout",
  "delete": "https://example.com/api/delete",
  "dataEndpoint": "https://example.com/api/data",
  "accountDeleteSupported": true
}
```

## Linked webpages

The `homepage`, `privacy`, `signup`, and `login` keys specify URLs that the
client may open in a web browser.

When these URLs are opened, the following values will be appended to the query
string:

| Name                 | Description                                                                                        |
| -------------------- | -------------------------------------------------------------------------------------------------- |
| `gianniappBaseV`     | Version of `gianniapp-base` used by the client. Not present if the client is not the game.         |
| `gianniappLang`      | Language code of the client. May not be present.                                                   |
| `gianniappDark`      | `true` if dark mode is enabled, `false` if it is disabled. May not be present.                     |
| `gianniappMiddlerV`  | Version of the middler used by the client, or version of the client if the client is not the game. |
| `gianniappMiddler`   | Name of the middler used by the client, or name of the client if the client is not the game.       |
| `giannisyncV`        | *gianniSync* spec version supported by the client.                                                 |
| `giannisyncRedirect` | May be present in requests to the `login` URL. See [Authentication](authentication.md).            |